package com.assignment.payment.services;

import com.assignment.payment.entities.PaymentEntity;
import com.assignment.payment.entities.PaymentTypeEntity;

import java.util.List;
import java.util.Optional;

public interface PaymentTypeService {
    List<PaymentTypeEntity> getAllPaymentType();

    Optional<PaymentTypeEntity> getById(int id);
    PaymentTypeEntity createPaymentType(PaymentTypeEntity payment);
    PaymentTypeEntity updatePaymentType(PaymentTypeEntity updatedPayment);
    void deletePaymentTypeById(Long paymentId);
}
