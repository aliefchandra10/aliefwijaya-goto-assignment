package com.assignment.payment.services;

import com.assignment.payment.entities.PaymentEntity;
import com.assignment.payment.repositories.PaymentRepository;
import com.assignment.payment.repositories.PaymentTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class PaymentServiceImpl implements PaymentService{
    private final PaymentRepository paymentRepository;
    private final PaymentTypeRepository paymentTypeRepository;

    @Autowired
    public PaymentServiceImpl(PaymentRepository paymentRepository, PaymentTypeRepository paymentTypeRepository) {
        this.paymentRepository = paymentRepository;
        this.paymentTypeRepository = paymentTypeRepository;
    }

    @Override
    public List<PaymentEntity> getAllPayment() {
        return paymentRepository.findAll();
    }

    public PaymentEntity createPayment(PaymentEntity payment) {
        return paymentRepository.save(payment);
    }

    public PaymentEntity getPaymentById(Long paymentId) {
        return paymentRepository.findById(Math.toIntExact(paymentId)).orElse(null);
    }

    public PaymentEntity updatePayment(PaymentEntity updatedPayment) {
        Long paymentId = updatedPayment.getPaymentId();
        PaymentEntity existingPayment = paymentRepository.findById(Math.toIntExact(paymentId))
                .orElseThrow(() -> new NoSuchElementException("Payment not found with ID : " + paymentId));

        existingPayment.setAmount(updatedPayment.getAmount());
        existingPayment.setPaymentType(updatedPayment.getPaymentType());
        existingPayment.setDate(updatedPayment.getDate());
        existingPayment.setCustomerId(updatedPayment.getCustomerId());

        return paymentRepository.save(existingPayment);
    }

    public void deletePaymentById(Long paymentId) {
        paymentRepository.deleteById(Math.toIntExact(paymentId));
    }

    public List<PaymentEntity> getFilteredPayments(
            Long customerId,
            String typeName,
            Date date,
            Double amount,
            Integer page,
            Integer size
    ) {
        if (page == null || page < 1) {
            page = 1;
        }

        if (size == null || size < 1) {
            size = 10;
        }

        return paymentRepository.getFilteredPayments(customerId, typeName, date, amount, page, size);
    }
}
