package com.assignment.payment.services;

import com.assignment.payment.entities.PaymentTypeEntity;
import com.assignment.payment.repositories.PaymentTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class PaymentTypeServiceImpl implements PaymentTypeService {
    private final PaymentTypeRepository paymentTypeRepository;

    @Autowired
    public PaymentTypeServiceImpl(PaymentTypeRepository paymentTypeRepository) {
        this.paymentTypeRepository = paymentTypeRepository;
    }

    @Override
    public List<PaymentTypeEntity> getAllPaymentType() {
        return paymentTypeRepository.findAll();
    }

    public Optional<PaymentTypeEntity> getById(int id) {
        return paymentTypeRepository.findById(id);
    }

    public PaymentTypeEntity createPaymentType(PaymentTypeEntity payment) {
        return paymentTypeRepository.save(payment);
    }

    public PaymentTypeEntity updatePaymentType(PaymentTypeEntity updatedPaymentType) {
        Long paymentId = updatedPaymentType.getpaymentTypeId();
        PaymentTypeEntity existingPaymentType = paymentTypeRepository.findById(Math.toIntExact(paymentId))
                .orElseThrow(() -> new NoSuchElementException("Payment Type not found with ID : " + paymentId));

        existingPaymentType.setTypeName(updatedPaymentType.getTypeName());
        return paymentTypeRepository.save(existingPaymentType);
    }

    public void deletePaymentTypeById(Long paymentTypeId) {
        paymentTypeRepository.deleteById(Math.toIntExact(paymentTypeId));
    }

}
