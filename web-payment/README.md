Candidate Name  :  Alief Chandra Wijaya
Position        :  Sr. Software Engineer (Engineering Productivity) Core R&D

dev environment :
nodejs v20.5.0
npm 9.8.0
vscode

In this project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\

### `npm install -g serve`
For installing all dependencies required for build/production mode

### `serve -s build`
For running the app in production mode


For filtering search , please provide at least customerID and PaymentType is filled