import React from 'react';
import PaymentData from './component/PaymentData';

const App = () => {
  return (
    <div>
      <PaymentData />
    </div>
  );
};

export default App;
