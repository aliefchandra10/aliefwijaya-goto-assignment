package com.assignment.payment.repositories;

import com.assignment.payment.entities.PaymentTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentTypeRepository extends JpaRepository<PaymentTypeEntity,Integer> {
}
