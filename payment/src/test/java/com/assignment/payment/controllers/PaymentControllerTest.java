package com.assignment.payment.controllers;
import static org.mockito.Mockito.*;

import java.util.Date;

import com.assignment.payment.entities.PaymentEntity;
import com.assignment.payment.entities.PaymentTypeEntity;
import com.assignment.payment.services.PaymentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(PaymentController.class)
public class PaymentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PaymentService paymentService;

    private PaymentEntity testPayment;

    @BeforeEach
    public void setup() {
        // Initialize testPayment object
        testPayment = new PaymentEntity();

        // Set up mock behavior for paymentService.createPayment()
        when(paymentService.createPayment(any(PaymentEntity.class))).thenReturn(testPayment);
    }

    @Test
    public void testCreatePayment() throws Exception {
        // Prepare the JSON payload for the request
        String requestBody = "{ \"amount\": 100.0, \"paymentType\": { \"typeName\": \"VirtualAccount\",\"paymentTypeId\": \"1\" }, \"date\": \"2023-08-02\", \"customerId\": 12345 }";

        // Send the POST request to the controller
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/payment/createPayment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.paymentId").value(testPayment.getPaymentId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.amount").value(testPayment.getAmount()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.paymentType.paymentTypeId").value(testPayment.getPaymentType().getpaymentTypeId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.paymentType.typeName").value(testPayment.getPaymentType().getTypeName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value(testPayment.getDate()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.customerId").value(testPayment.getCustomerId()));

        // Verify that paymentService.createPayment() was called with the correct argument
        verify(paymentService, times(1)).createPayment(any(PaymentEntity.class));
    }
}

