package com.assignment.payment.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "payment_type")
public class PaymentTypeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "payment_type_id")
    private Long paymentTypeId;

    @Column(name = "type_name")
    private String typeName;

    public Long getpaymentTypeId() {
        return paymentTypeId;
    }

    public void setpaymentTypeId(Long paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
