Candidate Name  :  Alief Chandra Wijaya
Position        :  Sr. Software Engineer (Engineering Productivity) Core R&D

dev environment :
openjdk-20
IntelliJ IDEA

In this project directory, you can run:

## mvn clean install
 command is used to build and package the project, ensuring that the code is compiled, and artifacts are generated 

## mvn spring-boot:run
command is used to run a Spring Boot application

-----------------------------------------------------

For Pagination, I choose pagination in database-level using stored procedure and front-end-side (react)
reason :
to reduce direct-load in database
flow :
front-end provide page size
springboot provide API to call the stored procedure that accept paginantion param (page number and size)
postgre process requested data using stored procedure for pagination logic and return the data efficiently

and also I made some indexes in Payment and PaymentType tables, that will impact on :
    - faster on reading/retrieve data, thus trade-offs....
    - slightly slower on writing data

----------------------------------------------------
API Specification :

## Get Payment by ID
method : GET
url : api/v1/payment/{id}
field : - id (Long)
description : to retrieve payment data by ID
sample request :
api/v1/payment/5

## Get Payment by Filter
method : GET
url : api/v1/payment/findPayments/RequestBodyParam
RequestBodyParam : - customerId (Long)
                   - typeName (varchar)
                   - amount (double/float)
                   - date (DATE -> YYYY-MM-DD)
                   - page (Int) => for page location
                   - size (Int) => for how much data per page
description : to retrieve payment data by multi filter including pagination
sample request :
api/v1/payment/findPayments?customerId=12345&typeName=Virtual Account&page=1&size=3&amount=100.0&date=2023-08-02

## Create Payment
method : POST
url : api/v1/payment/createPayment
description : to create new payment data (single or bulk)
field : - amount (double/float)
        - paymentTypeId (Long)
        - date (DATE -> YYYY-MM-DD)
        - customerId (Long)
sample body request (single) :
    [
        {
            "amount": 100.0,
            "paymentType": {
                "paymentTypeId": "6"
            },
            "date": "2023-08-02",
            "customerId": 1234
        }
    ]
sample body request (bulk) :
    [
        {
            "amount": 100.0,
            "paymentType": {
                "paymentTypeId": "6"
            },
            "date": "2023-08-02",
            "customerId": 1234
        },
        {
            "amount": 200.0,
            "paymentType": {
                "paymentTypeId": "5"
            },
            "date": "2023-08-03",
            "customerId": 12345
        }
    ]

## Update Payment
method : PUT
url : api/v1/payment/{id}
description : to edit payment data
field : - id (Long)
        - paymentId (Long)
        - amount (double/float)
        - paymentTypeId (Long)
        - date (DATE -> YYYY-MM-DD)
        - customerId (Long)
sample body request :
    {
        "paymentId":4,
        "amount": 1000000.50,
        "paymentType": {
            "paymentTypeId": 1,
        },
        "date": "2023-08-02",
        "customerId": 123456
    }

## Delete Payment by ID
method : DELETE
url : api/v1/payment/{id}
field : - id (Long)
description : to delete payment data by ID

## Get All PaymentType 
method : GET
url : api/v1/paymentType
field : 
description : to retrieve payment type
sample request :
api/v1/paymentType

## Create PaymentType
method : POST
url : api/v1/paymentType/createPaymentType
description : to create new payment type data
field : - typeName (varchar)
sample body request  :
{
    "typeName": "6"
}