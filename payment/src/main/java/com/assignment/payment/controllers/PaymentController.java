package com.assignment.payment.controllers;

import com.assignment.payment.entities.PaymentEntity;
import com.assignment.payment.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("api/v1/payment")
public class PaymentController {
    private final PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping("/createPayment")
    public List<PaymentEntity> createPayments(@RequestBody List<PaymentEntity> payments) {
        List<PaymentEntity> createdPayments = new ArrayList<>();

        for (PaymentEntity payment : payments) {
            PaymentEntity createdPayment = paymentService.createPayment(payment);
            createdPayments.add(createdPayment);
        }

        return createdPayments;
    }

    @GetMapping("/{paymentId}")
    public ResponseEntity<PaymentEntity> getPaymentById(@PathVariable Long paymentId) {
        PaymentEntity payment = paymentService.getPaymentById(paymentId);
        if (payment != null) {
            return ResponseEntity.ok(payment);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = "/getAllPayment")
    public ResponseEntity<List<PaymentEntity>> getAllPayment() {
        List<PaymentEntity> ListPayment = paymentService.getAllPayment();
        if (ListPayment.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ListPayment);
    }

    @GetMapping("/findPayments")
    public ResponseEntity<List<PaymentEntity>> getFilteredPayments(
            @RequestParam Long customerId,
            @RequestParam String typeName,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date,
            @RequestParam(required = false) Double amount,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size
    ) {
        List<PaymentEntity> paymentResults = paymentService.getFilteredPayments(customerId, typeName, date, amount, page, size);
        return ResponseEntity.ok(paymentResults);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PaymentEntity> updatePayment(@PathVariable("id") Long paymentId,
                                                 @RequestBody PaymentEntity updatedPayment) {
        if (!paymentId.equals(updatedPayment.getPaymentId())) {
            return ResponseEntity.badRequest().build();
        }

        PaymentEntity result = paymentService.updatePayment(updatedPayment);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{paymentId}")
    public ResponseEntity<String> deletePayment(@PathVariable Long paymentId) {
        paymentService.deletePaymentById(paymentId);
        return ResponseEntity.ok("Payment with ID " + paymentId + " has been deleted.");
    }
}

