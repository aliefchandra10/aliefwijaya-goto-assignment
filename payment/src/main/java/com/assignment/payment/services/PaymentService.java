package com.assignment.payment.services;

import com.assignment.payment.entities.PaymentEntity;

import java.util.Date;
import java.util.List;

public interface PaymentService {
    PaymentEntity createPayment(PaymentEntity payment);
    PaymentEntity getPaymentById(Long paymentId);
    PaymentEntity updatePayment(PaymentEntity updatedPayment);
    void deletePaymentById(Long paymentId);
    public List<PaymentEntity> getAllPayment();
    public List<PaymentEntity> getFilteredPayments(
            Long customerId,
            String typeName,
            Date date,
            Double amount,
            Integer page,
            Integer size
    );
}
