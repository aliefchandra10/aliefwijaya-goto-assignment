--
-- PostgreSQL database dump
--

-- Dumped from database version 14.8
-- Dumped by pg_dump version 14.8

-- Started on 2023-08-09 01:41:02

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 226 (class 1255 OID 16473)
-- Name: get_filtered_payments(bigint, character varying, date, double precision, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_filtered_payments(mandatory_customer_id bigint, mandatory_type_name character varying, optional_date date DEFAULT NULL::date, optional_amount double precision DEFAULT NULL::double precision, optional_page_num integer DEFAULT 1, optional_page_size integer DEFAULT 10) RETURNS TABLE(payment_id bigint, amount double precision, payment_type_id bigint, date date, customer_id bigint, type_name character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
    RETURN QUERY
    SELECT
        p.payment_id,
        p.amount,
        p.payment_type_id,
        p.date,
        p.customer_id,
        pt.type_name
    FROM
        payment p
    JOIN payment_type pt ON p.payment_type_id = pt.payment_type_id
    WHERE
        p.customer_id = mandatory_customer_id
        AND pt.type_name = mandatory_type_name
        AND (optional_date IS NULL OR p.date = optional_date)
        AND (optional_amount IS NULL OR p.amount = optional_amount)
    ORDER BY
        p.payment_id
    LIMIT optional_page_size
    OFFSET (optional_page_num - 1) * optional_page_size;
END;
$$;


ALTER FUNCTION public.get_filtered_payments(mandatory_customer_id bigint, mandatory_type_name character varying, optional_date date, optional_amount double precision, optional_page_num integer, optional_page_size integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 210 (class 1259 OID 16448)
-- Name: inventory; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.inventory (
    price double precision,
    quantity integer,
    item_id bigint NOT NULL,
    item_name character varying(255)
);


ALTER TABLE public.inventory OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16447)
-- Name: inventory_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.inventory_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inventory_item_id_seq OWNER TO postgres;

--
-- TOC entry 3332 (class 0 OID 0)
-- Dependencies: 209
-- Name: inventory_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.inventory_item_id_seq OWNED BY public.inventory.item_id;


--
-- TOC entry 212 (class 1259 OID 16455)
-- Name: payment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment (
    amount double precision,
    date date,
    customer_id bigint,
    payment_id bigint NOT NULL,
    payment_type_id bigint
);


ALTER TABLE public.payment OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16454)
-- Name: payment_payment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.payment_payment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_payment_id_seq OWNER TO postgres;

--
-- TOC entry 3333 (class 0 OID 0)
-- Dependencies: 211
-- Name: payment_payment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.payment_payment_id_seq OWNED BY public.payment.payment_id;


--
-- TOC entry 214 (class 1259 OID 16462)
-- Name: payment_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment_type (
    payment_type_id bigint NOT NULL,
    type_name character varying(255)
);


ALTER TABLE public.payment_type OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16461)
-- Name: payment_type_payment_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.payment_type_payment_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_type_payment_type_id_seq OWNER TO postgres;

--
-- TOC entry 3334 (class 0 OID 0)
-- Dependencies: 213
-- Name: payment_type_payment_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.payment_type_payment_type_id_seq OWNED BY public.payment_type.payment_type_id;


--
-- TOC entry 3175 (class 2604 OID 16451)
-- Name: inventory item_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventory ALTER COLUMN item_id SET DEFAULT nextval('public.inventory_item_id_seq'::regclass);


--
-- TOC entry 3176 (class 2604 OID 16458)
-- Name: payment payment_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment ALTER COLUMN payment_id SET DEFAULT nextval('public.payment_payment_id_seq'::regclass);


--
-- TOC entry 3177 (class 2604 OID 16465)
-- Name: payment_type payment_type_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_type ALTER COLUMN payment_type_id SET DEFAULT nextval('public.payment_type_payment_type_id_seq'::regclass);


--
-- TOC entry 3179 (class 2606 OID 16453)
-- Name: inventory inventory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inventory
    ADD CONSTRAINT inventory_pkey PRIMARY KEY (item_id);


--
-- TOC entry 3184 (class 2606 OID 16460)
-- Name: payment payment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (payment_id);


--
-- TOC entry 3186 (class 2606 OID 16467)
-- Name: payment_type payment_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_type
    ADD CONSTRAINT payment_type_pkey PRIMARY KEY (payment_type_id);


--
-- TOC entry 3180 (class 1259 OID 16476)
-- Name: idx_customer_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_customer_id ON public.payment USING btree (customer_id);


--
-- TOC entry 3181 (class 1259 OID 16475)
-- Name: idx_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_date ON public.payment USING btree (date);


--
-- TOC entry 3182 (class 1259 OID 16474)
-- Name: idx_payment_type_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_payment_type_id ON public.payment USING btree (payment_type_id);


--
-- TOC entry 3187 (class 2606 OID 16468)
-- Name: payment fkkvolsaw3e4jg4ra05vu135cj9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment
    ADD CONSTRAINT fkkvolsaw3e4jg4ra05vu135cj9 FOREIGN KEY (payment_type_id) REFERENCES public.payment_type(payment_type_id);


-- Completed on 2023-08-09 01:41:03

--
-- PostgreSQL database dump complete
--

