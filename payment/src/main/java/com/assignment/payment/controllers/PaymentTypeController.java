package com.assignment.payment.controllers;

import com.assignment.payment.entities.PaymentTypeEntity;
import com.assignment.payment.services.PaymentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("api/v1/paymentType")
public class PaymentTypeController {
    private final PaymentTypeService paymentTypeService;

    @Autowired
    public PaymentTypeController(PaymentTypeService paymentTypeService) {
        this.paymentTypeService = paymentTypeService;
    }

    @PostMapping("/createPaymentType")
    public PaymentTypeEntity createPaymentType(@RequestBody PaymentTypeEntity paymentType) {
        return paymentTypeService.createPaymentType(paymentType);
    }

    @GetMapping(value = "/getAllPaymentType")
    public ResponseEntity<List<PaymentTypeEntity>> getAllPaymentType() {
        List<PaymentTypeEntity> ListPaymentType = paymentTypeService.getAllPaymentType();
        if (ListPaymentType.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ListPaymentType);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PaymentTypeEntity> updatePaymentType(@PathVariable("id") Long paymentTypeId,
                                                       @RequestBody PaymentTypeEntity updatedPaymentType) {
        if (!paymentTypeId.equals(updatedPaymentType.getpaymentTypeId())) {
            return ResponseEntity.badRequest().build();
        }

        PaymentTypeEntity result = paymentTypeService.updatePaymentType(updatedPaymentType);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{paymentTypeId}")
    public ResponseEntity<String> deletePaymentType(@PathVariable Long paymentTypeId) {
        paymentTypeService.deletePaymentTypeById(paymentTypeId);
        return ResponseEntity.ok("Payment Type with ID " + paymentTypeId + " has been deleted.");
    }
}

