package com.assignment.payment.repositories;

import com.assignment.payment.entities.PaymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface PaymentRepository extends JpaRepository<PaymentEntity,Integer> {
    @Query(nativeQuery = true, value = "SELECT * FROM get_filtered_payments(:customerId, :typeName, :date, :amount, :page, :size)")
    List<PaymentEntity> getFilteredPayments(
            @Param("customerId") Long customerId,
            @Param("typeName") String typeName,
            @Param("date") Date date,
            @Param("amount") Double amount,
            @Param("page") Integer page,
            @Param("size") Integer size
    );
}
