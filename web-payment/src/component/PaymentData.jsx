import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { debounce } from 'lodash';

const PaymentData = () => {
    const [customerId, setCustomerId] = useState('');
    const [paymentTypes, setPaymentTypes] = useState([]);
    const [date, setdate] = useState('');
    const [amount, setamount] = useState('');
    const [results, setResults] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [size, setsize] = useState(2);
    const [errorMessage, setErrorMessage] = useState('');
    const [selectedPaymentType, setSelectedPaymentType] = useState('');
    const [typeName, setTypeName] = useState('');
    const startIndex = (currentPage - 1) * size;
    const endIndex = startIndex + size;

    useEffect(() => {
      axios
          .get('http://localhost:8080/api/v1/paymentType/getAllPaymentType')
          .then((response) => {
              setPaymentTypes(response.data);
          })
          .catch((error) => {
            setErrorMessage('An error occurred while fetching paymentTypes.');
          });
   }, []);

    const handleSearch = () => {
        if (!customerId && typeName==="" && !date && !amount) {
          setResults([]); 
          setErrorMessage(''); 
          return;
        }
        if (!customerId || typeName==="") {
          setResults([]); 
          setErrorMessage('Please fill in customerID and Choose Payment Type'); 
          return;
        }
        axios
          .get('http://localhost:8080/api/v1/payment/findPayments', {
            params: {
              customerId,
              typeName,
              date,
              amount,
              size,
            },
          })
          .then((response) => {
            setResults(response.data);
            setErrorMessage('');
          })
          .catch((error) => {
            setErrorMessage('An error occurred while fetching data.');
          });
      };
  
    const debouncedSearch = debounce(handleSearch, 1300);
  
    const handleCustomerIdChange = (event) => {
      const { value } = event.target;
      setCustomerId(value);
      debouncedSearch();
    };
  
    const handleTypeNameChange = (event) => {
      const selectedValue = event.target.value;
      setSelectedPaymentType(selectedValue);
      
      if (selectedValue === "") {
          setTypeName(""); 
          debouncedSearch();
      } else {
          setTypeName(selectedValue);
          debouncedSearch();
      }
  };

    const handledateChange = (event) => {
        const { value } = event.target;
        setdate(value);
        debouncedSearch();
    };
    
    const handleamountChange = (event) => {
        const { value } = event.target;
        setamount(value);
        debouncedSearch();
    };

    const handleItemsPerPageChange = (event) => {
      const selectedsize = parseInt(event.target.value);
      setsize(selectedsize);
      debouncedSearch();
      setCurrentPage(1);
  };

    useEffect(() => {
      debouncedSearch();
    }, [customerId, typeName, date, amount, currentPage, size]);
  
    return (
      <div>
        <div>
          <label>Customer ID:</label>
          <input
            type="text"
            value={customerId}
            onChange={handleCustomerIdChange}
            placeholder="Enter Customer ID"
          />
        </div>
        <div>
            <label>Payment Type:</label>
            <select value={typeName} onChange={(e) => handleTypeNameChange(e)}>
                <option value="">Select Payment Type</option>
                {paymentTypes.map((paymentType) => (
                    <option key={paymentType.paymentTypeId} value={paymentType.typeName}>
                        {paymentType.typeName}
                    </option>
                ))}
            </select>
        </div>
        <div>
          <label>Date:</label>
          <input
            type="text"
            value={date}
            onChange={handledateChange}
            placeholder="Enter Date"
          />
        </div>
        <div>
          <label>Amount:</label>
          <input
            type="text"
            value={amount}
            onChange={handleamountChange}
            placeholder="Enter Amount"
          />
        </div>
        <div>
            <label>Items per Page:</label>
            <select onChange={handleItemsPerPageChange} value={size}>
                <option value="2">2</option>
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </div>

        {errorMessage && <p style={{ color: 'red' }}>{errorMessage}</p>}

        <div>
          <table style={{ border: "1px solid black", width: "80%" }}>
            <thead>
              <tr>
                <th style={{ border: "1px solid black", padding: "8px" }}>Payment ID</th>
                <th style={{ border: "1px solid black", padding: "8px" }}>Amount</th>
                <th style={{ border: "1px solid black", padding: "8px" }}>Payment Type</th>
                <th style={{ border: "1px solid black", padding: "8px" }}>Date</th>
                <th style={{ border: "1px solid black", padding: "8px" }}>Customer ID</th>
              </tr>
            </thead>
            <tbody>
              {results.slice(startIndex, endIndex).map((result) => (
                <tr key={result.paymentId}>
                  <td style={{ border: "1px solid black", padding: "8px" }}>{result.paymentId}</td>
                  <td style={{ border: "1px solid black", padding: "8px" }}>{result.amount}</td>
                  <td style={{ border: "1px solid black", padding: "8px" }}>{result.paymentType.typeName}</td>
                  <td style={{ border: "1px solid black", padding: "8px" }}>{result.date}</td>
                  <td style={{ border: "1px solid black", padding: "8px" }}>{result.customerId}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
  
        {results.length === 0 && <p>No results found.</p>}
        <div>
        <button
          onClick={() => setCurrentPage(currentPage - 1)}
          disabled={currentPage === 1}
        >
          Previous
        </button>
        <button
          onClick={() => setCurrentPage(currentPage + 1)}
          disabled={endIndex >= results.length}
        >
          Next
        </button>
      </div>
      </div>
    );
  };
  
  export default PaymentData;
  